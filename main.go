package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/minastephc/gopaasify/cmdline"
	"bitbucket.org/minastephc/gopaasify/config"
	"bitbucket.org/minastephc/gopaasify/docker"
	"bitbucket.org/minastephc/gopaasify/kubernetes"
)

var (
	// Version is the API version, set at compile time
	Version string
	// Build is the build number, set at compile time
	Build string
	// TemplateDir is the templates directory
	TemplateDir string
)

func main() {

	dir, err := os.Getwd()
	dir += "/tmpl"

	if err != nil {
		log.Fatal(err)
	}
	TemplateDir = dir

	cmdline := cmdline.New()
	cmdline.AddFlag("d", "debug", "Enable debug messages")
	cmdline.AddFlag("v", "version", "Display version")
	cmdline.AddOption("t", "template", "tmpldir", "Specify an alternate template directory")
	cmdline.AddArgument("config", "Configuration file")
	cmdline.Parse(os.Args)

	if cmdline.IsOptionSet("template") {
		TemplateDir = cmdline.OptionValue("template")
	}

	if cmdline.IsOptionSet("d") {
		config.SetDebug()
	}

	if cmdline.IsOptionSet("v") {
		fmt.Println(os.Args[0] + " " + Version + " - build " + Build[0:9])
		os.Exit(1)
	}

	configFile := cmdline.ArgumentValue("config")

	fmt.Println(os.Args[0] + " " + Version + " - build " + Build[0:9])

	var myTmpl *config.Templates
	myTmpl = config.SetConfig(TemplateDir)

	err2 := config.ValidateConfig(*myTmpl)
	if err2 != nil {
		log.Fatal(err2)
		os.Exit(1)
	}

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		fmt.Println("[CRIT] File does not exist:", configFile)
		os.Exit(1)
	} else {
		config.LoadConfigFromFile(configFile)
		// Now that the config is in memory, let's proceed
		config.MyConf.Global.Version = Version
		// Doker
		docker.CloneDir(config.MyConf)
		docker.MakeMakefile(config.MyConf, *myTmpl)
		docker.MakeDockerfile(config.MyConf, *myTmpl)
		docker.MakeJenkinsfile(config.MyConf, *myTmpl)
		docker.MakeVhost(config.MyConf, *myTmpl)

		// Kubernetes
		kubernetes.MakeService(config.MyConf, *myTmpl)
	}
}
