// Package docker provides the necessary functions to create the Makefile + Dockerfile + jenkinsfile.
package docker

// Imports
import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"text/template"

	"bitbucket.org/minastephc/gopaasify/config"
	"golang.org/x/crypto/ssh"
	git "gopkg.in/src-d/go-git.v4"
	gitssh "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

// Structs for templates

// Makefile struct
type Makefile struct {
	App     string
	Repo    string
	AppPath string
}

// Jenkinsfile struct
type Jenkinsfile struct {
	App   string
	Where string
}

// Dockerfile struct
type Dockerfile struct {
	App          string
	DockerRepo   string
	Image        string
	ImageVersion string
}

// CloneDir clone the source repository
func CloneDir(config config.Full) {
	method := strings.Split(config.Global.Gitbase, ":")[0]
	gitRepoSource := config.Global.Gitbase
	project := config.Global.Prj
	repo := config.Makefile.Repo
	if _, errdir := os.Stat(config.Global.Clone); os.IsNotExist(errdir) {
		_ = os.MkdirAll(config.Global.Clone, os.ModePerm)
	}
	cloneto := config.Global.Clone + "/" + config.Global.App
	var cloneFrom string
	if project != "" {
		cloneFrom = gitRepoSource + "/" + project + "/" + repo
	} else {
		cloneFrom = gitRepoSource + "/" + repo
	}
	var errCloneTo error
	fmt.Println("Cloning: ", cloneFrom, "- to:", cloneto)
	if method == "ssh" {
		sshkey, _ := ioutil.ReadFile(config.Global.Sshkey)
		signer, _ := ssh.ParsePrivateKey([]byte(sshkey))
		auth := &gitssh.PublicKeys{User: "git", Signer: signer}
		_, errCloneTo = git.PlainClone(cloneto, false, &git.CloneOptions{
			URL:      cloneFrom,
			Auth:     auth,
			Progress: os.Stdout,
		})
	} else {
		_, errCloneTo = git.PlainClone(cloneto, false, &git.CloneOptions{
			URL:      cloneFrom,
			Progress: os.Stdout,
		})
	}
	if errCloneTo != nil {
		fmt.Println("[CRIT] Cannot clone:", errCloneTo)
		os.Exit(1)
	}
}

// MakeMakefile creates the Makefile
func MakeMakefile(config config.Full, templates config.Templates) {
	mkTmpl := templates.Makefile
	var makefile Makefile
	makefile.App = config.Global.App
	makefile.AppPath = config.Makefile.Apppath
	makefile.Repo = config.Makefile.Repo
	tmpl := template.Must(template.ParseFiles(mkTmpl))
	destDir := config.Global.Clone + "/" + config.Makefile.Repo + "/build/docker"
	destFile := destDir + "/Makefile"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	mkf, errfic := os.Create(destFile)
	if errfic != nil {
		fmt.Println("[CRIT]", errfic)
		os.Exit(1)
	}
	w := io.Writer(mkf)
	tmpl.Execute(w, makefile)
	fmt.Println("File", destFile, "written successfully")
}

// MakeDockerfile creates the Dockerfile
func MakeDockerfile(config config.Full, templates config.Templates) {
	mkTmpl := templates.Dockerfile
	var dockerfile Dockerfile
	dockerfile.App = config.Global.App
	dockerfile.DockerRepo = config.Global.DockerRepo
	dockerfile.Image = config.Global.Image
	dockerfile.ImageVersion = config.Global.ImageVersion
	tmpl := template.Must(template.ParseFiles(mkTmpl))
	destDir := config.Global.Clone + "/" + config.Global.App + "/build/docker"
	destFile := destDir + "/Dockerfile"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	mkf, errfic := os.Create(destFile)
	if errfic != nil {
		fmt.Println("[CRIT]", errfic)
		os.Exit(1)
	}
	w := io.Writer(mkf)
	tmpl.Execute(w, dockerfile)
	fmt.Println("File", destFile, "written successfully")
}

// MakeJenkinsfile creates the Jenkinsfile
func MakeJenkinsfile(config config.Full, templates config.Templates) {
	mkTmpl := templates.Jenkinsfile
	var jenkinsfile Jenkinsfile
	jenkinsfile.App = config.Global.App
	jenkinsfile.Where = config.Jenkins.Where
	tmpl := template.Must(template.ParseFiles(mkTmpl))
	destDir := config.Global.Clone + "/" + config.Global.App
	destFile := destDir + "/Jenkinsfile"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	mkf, errfic := os.Create(destFile)
	if errfic != nil {
		fmt.Println("[CRIT]", errfic)
		os.Exit(1)
	}
	w := io.Writer(mkf)
	tmpl.Execute(w, jenkinsfile)
	fmt.Println("File", destFile, "written successfully")
}

// MakeVhost creates the vhost file
func MakeVhost(config config.Full, templates config.Templates) {
	destDir := config.Global.Clone + "/" + config.Global.App + "/build/docker"
	destFile := destDir + "/" + config.Global.App + ".conf"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	in, errSrc := os.Open(templates.Vhostfile)
	if errSrc != nil {
		fmt.Println("[CRIT]", errSrc)
		os.Exit(1)
	}
	defer in.Close()

	out, errDst := os.Create(destFile)
	if errDst != nil {
		fmt.Println("[CRIT]", errDst)
		os.Exit(1)
	}
	defer out.Close()

	_, errCp := io.Copy(out, in)
	if errCp != nil {
		fmt.Println("[CRIT]", errCp)
	}
	fmt.Println("File", destFile, "written successfully")
}
