// Package config provides the necessary structs and functions in order to create the configuration from the commandline and from the confguration file provided as parameter.
package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/fatih/structs"
	yaml "gopkg.in/yaml.v2"
)

// Variables
var (
	Debug   = false
	TmplDir = ""
	MyConf  Full
)

// Global configuration
type Global struct {
	App          string `yaml:"app"`
	Prj          string `yaml:"prj"`
	Clone        string `yaml:"clone"`
	Gitbase      string `yaml:"gitbase"`
	Sshkey       string `ymal:"sshkey"`
	Version      string `yaml:"version"`
	DockerRepo   string `yaml:"dockerrepo"`
	Image        string `yaml:"image"`
	ImageVersion string `yaml:"imageversion"`
}

// Jenkins parameters
type Jenkins struct {
	Where string `yaml:"where"`
}

// Makefile parameters
type Makefile struct {
	Repo    string `yaml:"repo"`
	Apppath string `yaml:"app_path"`
	Branch  string `yaml:"branch"`
}

// HPA parameter
type HPA struct {
	Minpods   int `yaml:"minpods"`
	Maxpods   int `yaml:"maxpods"`
	Cpuusage  int `yaml:"cpuusage"`
	Timescale int `yaml:"timescale"`
}

// Volume parameter
type Volume struct {
	Name      string `yaml:"vname"`
	Stor      string `yaml:"stor"`
	Nfssrv    string `yaml:"nfssrv"`
	Nfspath   string `yaml:"nfspath"`
	Mountpath string `yaml:"mountpath"`
}

// Secret parameter
type Secret struct {
	Name      string `yaml:"mname"`
	Sname     string `yaml:"sname"`
	Mountpath string `yaml:"smountpath"`
}

// Ingress parameter
type Ingress struct {
	Host string `yaml:"host"`
}

// Kubernetes configuration
type Kubernetes struct {
	Namespace string    `yaml:"namespace"`
	Tier      string    `yaml:"tier"`
	Replicas  int       `yaml:"replicas"`
	Newrelic  bool      `yaml:"newrelic"`
	Ingress   []Ingress `yaml:"ingress"`
	Hpa       HPA       `yaml:"hpa"`
	Volume    []Volume  `yaml:"vol"`
	Secret    []Secret  `yaml:"secret"`
}

// Templates list
type Templates struct {
	Makefile       string
	Dockerfile     string
	Jenkinsfile    string
	Vhostfile      string
	K8sdeploy      string
	K8sdepvmnt     string
	K8sdepvols     string
	K8sdepvoldec   string
	K8sdepvolsec   string
	K8sdepvolclaim string
	K8sservice     string
	K8singresshead string
	K8singresshost string
	K8shpa         string
	K8spv          string
	K8spvc         string
}

// Full configuration
type Full struct {
	Global     Global     `yaml:"global"`
	Makefile   Makefile   `yaml:"makefile"`
	Jenkins    Jenkins    `yaml:"jenkinsfile"`
	Kubernetes Kubernetes `yaml:"k8s"`
}

// SetDebug enables/disables debug mode
func SetDebug() {
	Debug = !Debug
}

// SetConfig sets the configuration elements depending on the template directory
func SetConfig(templateDir string) *Templates {
	templates := new(Templates)
	templates.Makefile = templateDir + "/docker/Makefile"
	templates.Dockerfile = templateDir + "/docker/Dockerfile"
	templates.Jenkinsfile = templateDir + "/docker/Jenkinsfile"
	templates.Vhostfile = templateDir + "/docker/vhost.conf"
	templates.K8sdeploy = templateDir + "/k8s/deploy_header.yml"
	templates.K8sdepvmnt = templateDir + "/k8s/deploy_volmnt.yml"
	templates.K8sdepvols = templateDir + "/k8s/deploy_volumes.yml"
	templates.K8sdepvoldec = templateDir + "/k8s/deploy_voldec.yml"
	templates.K8sdepvolsec = templateDir + "/k8s/deploy_vol_sec.yml"
	templates.K8sdepvolclaim = templateDir + "/k8s/deploy_vol_claim.yml"
	templates.K8sservice = templateDir + "/k8s/service.yml"
	templates.K8singresshead = templateDir + "/k8s/ingress_header.yml"
	templates.K8singresshost = templateDir + "/k8s/ingress_hosts.yml"
	templates.K8shpa = templateDir + "/k8s/hpa.yml"
	templates.K8spv = templateDir + "/k8s/pv.yml"
	templates.K8spvc = templateDir + "/k8s/pvc.yml"
	return (templates)
}

// ValidateConfig check that all template files are present
func ValidateConfig(templates Templates) error {
	// Check that all templates are present
	hasError := false
	s := structs.New(templates)
	if Debug {
		fmt.Println("----------")
	}
	for _, f := range s.Fields() {
		ficName := f.Value().(string)
		if _, err := os.Stat(ficName); os.IsNotExist(err) {
			fmt.Println("[CRIT]", f.Name(), "-> File does not exist:", ficName)
			hasError = true
		} else {
			if Debug {
				fmt.Println("[INFO]", f.Name(), "-> File exists:", ficName)
			}
		}
	}
	if hasError {
		return (errors.New("One or more template file is not present"))
	}
	return (nil)
}

// LoadConfigFromFile reads the given config file into structs
func LoadConfigFromFile(configFile string) {
	if Debug {
		fmt.Println("----------")
		fmt.Println("[INFO] Config file is:", configFile)
		fmt.Println("----------")
	}

	source, errcfg := ioutil.ReadFile(configFile)
	if errcfg != nil {
		panic(errcfg)
	}

	erryaml := yaml.Unmarshal(source, &MyConf)
	if erryaml != nil {
		panic(erryaml)
	}
	if Debug {
		fmt.Println("Global information:")
		fmt.Println("[INFO] Base git URL:", MyConf.Global.Gitbase)
		fmt.Println("[INFO] Git project:", MyConf.Global.Prj)
		fmt.Println("[INFO] Directory to clone to:", MyConf.Global.Clone)
		fmt.Println("[INFO] SSH key:", MyConf.Global.Sshkey)
		fmt.Println("[INFO] Application name:", MyConf.Global.App)
		fmt.Println("-----")
		fmt.Println("Makefile information:")
		fmt.Println("[INFO] Repository to clone:", MyConf.Makefile.Repo)
		fmt.Println("[INFO] Application path:", MyConf.Makefile.Apppath)
		var branchInfo string
		if MyConf.Makefile.Branch == "" {
			branchInfo = "[default: master]"
		} else {
			branchInfo = MyConf.Makefile.Branch
		}
		fmt.Println("[INFO] Branch to clone:", branchInfo)

		fmt.Println("-----")
		fmt.Println("Jenkinsfile information:")
		fmt.Println("[INFO] Where to compile:", MyConf.Jenkins.Where)

		if MyConf.Kubernetes.Namespace != "" {
			fmt.Println("-----")
			fmt.Println("Kubernetes information:")
			fmt.Println("[INFO] Namespace:", MyConf.Kubernetes.Namespace)
			fmt.Println("[INFO] Tier:", MyConf.Kubernetes.Tier)
			if MyConf.Kubernetes.Replicas == 0 {
				MyConf.Kubernetes.Replicas = 2
			}
			fmt.Println("[INFO] Number of replicas for pod:", MyConf.Kubernetes.Replicas)
			if !MyConf.Kubernetes.Newrelic {
				MyConf.Kubernetes.Newrelic = false
			}
			fmt.Println("[INFO] Enable New Relic:", strconv.FormatBool(MyConf.Kubernetes.Newrelic))
			fmt.Print("[INFO] Ingress host(s): ")
			for host := range MyConf.Kubernetes.Ingress {
				fmt.Print(MyConf.Kubernetes.Ingress[host].Host)
				fmt.Print(" ")
			}
			fmt.Print("\n")
			if MyConf.Kubernetes.Hpa.Cpuusage != 0 {
				fmt.Println("[INFO] HPA Min number of pods:", MyConf.Kubernetes.Hpa.Minpods)
				fmt.Println("[INFO] HPA Max number of pods:", MyConf.Kubernetes.Hpa.Maxpods)
				fmt.Println("[INFO] HPA CPU Usage:", MyConf.Kubernetes.Hpa.Cpuusage)
				fmt.Println("[INFO] HPA Time scale:", MyConf.Kubernetes.Hpa.Timescale)
			}
			if len(MyConf.Kubernetes.Volume) != 0 {
				var num int
				for vol := range MyConf.Kubernetes.Volume {
					num++
					fmt.Println("[INFO] Volume", num, "name:", MyConf.Kubernetes.Volume[vol].Name)
					fmt.Println("[INFO] Volume", num, "storage:", MyConf.Kubernetes.Volume[vol].Stor)
					fmt.Println("[INFO] Volume", num, "mount path:", MyConf.Kubernetes.Volume[vol].Mountpath)
					fmt.Println("[INFO] Volume", num, "NFS server:", MyConf.Kubernetes.Volume[vol].Nfssrv)
					fmt.Println("[INFO] Volume", num, "path on NFS server:", MyConf.Kubernetes.Volume[vol].Nfspath)
				}
			}
			if len(MyConf.Kubernetes.Secret) != 0 {
				var numsec int
				for sec := range MyConf.Kubernetes.Secret {
					numsec++
					fmt.Println("[INFO] Secret", numsec, "mount name:", MyConf.Kubernetes.Secret[sec].Name)
					fmt.Println("[INFO] Secret", numsec, "secret name:", MyConf.Kubernetes.Secret[sec].Sname)
					fmt.Println("[INFO] Secret", numsec, "mount path:", MyConf.Kubernetes.Secret[sec].Mountpath)
				}
			}
		} else {
			fmt.Println("-----")
			fmt.Println("No Kubernetes configuration found")
		}
		fmt.Println("----------")
	}
}
