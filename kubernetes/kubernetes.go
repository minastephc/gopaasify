// Package kubernetes provides the necessary structs and functions in order to create the kubernetes objects definitions in yaml files.
package kubernetes

// Imports
import (
	"fmt"
	"html/template"
	"io"
	"os"
	"strconv"

	"bitbucket.org/minastephc/gopaasify/config"
)

// Structs for templates

// DeployHeader struct
type DeployHeader struct {
	App       string
	Namespace string
	Replicas  string
	Tier      string
	Version   string
	Newrelic  string
}

// Service struct
type Service struct {
	App       string
	Namespace string
	Tier      string
}

// MakeDeploy makes the deploy.yml file
func MakeDeploy(config config.Full, templates config.Templates) {
	tmpl := templates.K8sdeploy
	var deployHeader DeployHeader
	deployHeader.App = config.Global.App
	deployHeader.Namespace = config.Kubernetes.Namespace
	deployHeader.Tier = config.Kubernetes.Tier
	deployHeader.Replicas = strconv.Itoa(config.Kubernetes.Replicas)
	deployHeader.Version = config.Global.Version
	deployHeader.Newrelic = strconv.FormatBool(config.Kubernetes.Newrelic)

	destDir := config.Global.Clone + "/" + config.Global.App + "/k8s"
	destFile := destDir + "/deploy.yml"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	mkf, errfic := os.Create(destFile)
	if errfic != nil {
		fmt.Println("[CRIT]", errfic)
		os.Exit(1)
	}
	tmplt := template.Must(template.ParseFiles(tmpl))
	w := io.Writer(mkf)
	tmplt.Execute(w, deployHeader)
	fmt.Println("File", destFile, "written successfully")

}

// MakeService makes the service.yml file
func MakeService(config config.Full, templates config.Templates) {
	tmpl := templates.K8sservice
	var service Service
	service.App = config.Global.App
	service.Namespace = config.Kubernetes.Namespace
	service.Tier = config.Kubernetes.Tier

	destDir := config.Global.Clone + "/" + config.Global.App + "/k8s"
	destFile := destDir + "/service.yml"
	if _, errdir := os.Stat(destDir); os.IsNotExist(errdir) {
		_ = os.MkdirAll(destDir, os.ModePerm)
	}
	mkf, errfic := os.Create(destFile)
	if errfic != nil {
		fmt.Println("[CRIT]", errfic)
		os.Exit(1)
	}
	tmplt := template.Must(template.ParseFiles(tmpl))
	w := io.Writer(mkf)
	tmplt.Execute(w, service)
	fmt.Println("File", destFile, "written successfully")

}
