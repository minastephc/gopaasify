BINARY=kubernetize

VERSION=`cat VERSION.txt`
BUILD=`git rev-parse HEAD`

LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD}"

.DEFAULT_GOAL: ${BINARY}

${BINARY}:
#	go get ./...
	go build -v ${LDFLAGS} -o ${BINARY} .

get:
	# go get -v ./...
	go get -v .

docker:
#	go get ./...
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo ${LDFLAGS} -o ${BINARY} .
	docker build . -t ${BINARY}:${VERSION}

install:
	go install ${LDFLAGS} -o ${BINARY} .

clean:
	if [ -f ${BINARY} ] ; then rm -f ${BINARY} ; fi

cleandocker:
	docker container ls -q --filter "name=${BINARY}" | grep -q . && docker container rm -f ${BINARY} || echo "No container"
	docker image rm -f ${BINARY}
	docker image prune -f

.PHONY: clean install

